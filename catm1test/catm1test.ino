
#include "ArduinoJson.h"

int csn = 0; // current step number

String tagID = "AABBCCDD";
String serial = "FF40200141";
//String serial =   "UR00000000";

int currentPacketCount = 40;
int tryDelay = 30;
int tryDelay4 = 15;
int tryDelay7 = 50;

int httpFailCount = 0;

String jsonSent = "";
String tokenSent = "";

int rfcheck;

float currents[100];
int currentCount = 0;
int tryCount[10] = {0,0,0,0,0,0,0,0,0,0};
int retryCount[10] = {0,0,0,0,0,0,0,0,0,0};

bool act = true;

int KEY_PW = 35;
int VIN = 10;
int RTS = 37; /// need change
int CTS = 36; 
bool allowATC = true;

float testCurrent = 0.01;
int testBattery = 0;

int writeVolt(float volt){
  int analogW = (1.0*volt*255)/5;
  Serial.print("writeVoltAs : ");
  Serial.println(analogW);
  return analogW;
}

void boot(){
  Serial.println("Booting...");
  //analogWrite(VIN, writeVolt(4.2));
  //delay(500);
  //digitalWrite(KEY_PW,HIGH);
  digitalWrite(KEY_PW,HIGH);
  delay(1500);
  digitalWrite(KEY_PW,LOW);
  delay(1500);
  digitalWrite(KEY_PW,HIGH);
  //analogWrite(KEY_PW, writeVolt(0));
  //delay(900);
  //analogWrite(KEY_PW, writeVolt(1.8));
  Serial.println("Booting Done.");
}

void setup() {
  Serial.begin(115200);
  Serial3.begin(115200); //Cat M.1

  pinMode(KEY_PW,OUTPUT);
  pinMode(VIN,OUTPUT);
  
  //pinMode(RTS,OUTPUT);
  //pinMode(CTS,OUTPUT);
  
  rfcheck = 1; // for test

  boot();
  
}

String stringToHex(String input){
  String hex = "";
  for(int i = 0 ; i < input.length(); i++){
    char c = (char)input[i];
    hex+= String(c, HEX);
  }
  return hex;
}

String hexToString(String hex)
{
    String text = "";
     
    for(int k=0;k< hex.length();k++)
    {
      if(k%2!=0)
      {
        char temp[3];
        sprintf(temp,"%c%c",hex[k-1],hex[k]);
        int number = (int)strtol(temp, NULL, 16);
        text+=char(number);
        
      }
    }  
    return text;
}



void tStep(int sn){
  tryCount[sn] = 0;
  //return;
  if(rfcheck==1 && allowATC){
    setATC(false);
    csn=sn+1;
    //delay(100);
    if(sn==0){
      Serial3.println("at+cereg?");
      //Serial3.println("AT$$DBS");
      //Serial.println("cs1");
      //Serial3.println("AT+CFUN?");
      //Serial3.println("at+cereg?");
    }
    else if(sn==1){
      Serial3.println("AT+CFUN?");
      //Serial3.println("at*rndisdata=0");
    }
    else if(sn==2){
      
      Serial3.println("at*rndisdata=1");
    }
    else if(sn==3){
      
      Serial3.println("at+wsocr=0,13.125.162.166,9001,1,1");
    }
    else if(sn==4){
      testCurrent = -0.1;
      Serial3.println("at+wsoco=0");
    }
  }
  else{
    csn=0;
    //delay(100);
  }
  

}

void resetCatM1(){
  Serial.println("resetCatM1");
  Serial3.println("AT+WSOCL=0");
  delay(500);
  //Serial3.println("AT+CFUN=6"); // USIM 초기화 후
  tStep(0);// 처음부터 다시 초기화
}

//OK
//+WSOCO:0,OPEN_CMPL
//sendCatm1JSONURLEncoded = %7B%22currents%22%3A%5B%5D%2C%22RFID%22%3A%22AA+BB+CC+DD%22%2C%22serial%22%3A%22FF00000001%22%7D
//at+wsowr=0,200,474554202f4849454e5359532f4361744d312f6368617267+WSORD=0,1046,485454502f312e3120323030204f4b0d0a5365727665723a206e67696e780d0a446174653a204672692c203034204a616e20323031392030353a34303a353220474d540d0a436f6e74656e742d547970653a20746578742f68746d6c3b20636861727365743d7574662d380d0a5472616e736665722d456e636f64696e673a206368756e6b65640d0a436f6e6e656374696f6e3a206b6565702d616c6976650d0a566172793a204163636570742d456e636f64696e670d0a5033503a2043503d274e4f4920435552612041444d6120444556612054414961204f55522044454c612042555320494e4420504859204f4e4c20554e4920434f4d204e415620494e542044454d20505245270d0a582d506f77657265642d42793a205048502f352e332e313370310d0a0d0a64620d0a6a736f6e203d207b5c2263757272656e74735c223a5b5d2c5c22524649445c223a5c2241412042422043432044445c222c5c2273657269616c5c223a5c22464630303030303030315c227d3c62722f3e75726c6465636f6465203d207b5c2263757272656e74735c223a5b5d2c5c22524649445c223a5c2241412042422043432044445c222c5c2273657269616c5c223a5c22464630303030303030315c227d3c62722f3e52464944203d2041412042422043432044443c62722f3e73657269616c203d20464630303030303030313c62722f3e737563636573730d0a
//+WSORD=0,10,300d0a0d0a

String convertMsg(String str, String cmd){
  //+wsowr=
    //int first = str.indexOf("+WSORD=");
    int first = str.indexOf(cmd);
    int second = str.indexOf(",",first+1);
    //int strlength = str.length(); // 
    int third = str.indexOf(",",second+1); // 
    int fourth = str.indexOf(",",third+1); // 
    //String str1 = str.substring(1, first); //
    //String str2 = str.substring(first+1, second); //
    //String str3 = str.substring(second+1, third); //
    String str4 = str.substring(third+1, fourth); //
    //Serial.println(str1);
    //Serial.println(str2);
    //Serial.println(str3);
    //Serial.println(str4);

    String result = hexToString(str4);
    Serial.println(result);
    return result;
}


void sendCatm1Token(String tokenString){
    tokenSent = tokenString;
    Serial.println("sendCatm1Token = "+tokenString);

    //delay(500); // charge energy

    String tr1 = stringToHex("GET /CatM1/charge_token.php?token=");
    //String tr1 = stringToHex("GET /CatM1/socket_test.php?json=");
    String tr2 = stringToHex(tokenString);
    
    String tr3 = stringToHex(" HTTP/1.1");
    tr3+="0d0a";
    String tr4 = stringToHex("HOST: 13.125.162.166");
    tr4+="0d0a";
    String tr5 = stringToHex("cache-control: no-cache");
    tr5+="0d0a0d0a";
    
    //String tr3 = "20485454502f312e310d0a484f53543a207961636861776d2e6361666532342e636f6d0d0a63616368652d636f6e74726f6c3a206e6f2d63616368650d0a0d0a";
    int trSize = tr1.length()+tr2.length()+tr3.length()+tr4.length()+tr5.length();
    String atinit = "at+wsowr=0,";
    atinit += trSize;
    atinit += ",";
    atinit += tr1;
    atinit += tr2;

    String strData = "";
    strData += tr3;
    strData += tr4;
    strData += tr5;
    
//    Serial.print(atinit);
//    Serial.println(strData);
//    
//    Serial3.print(atinit);
//    Serial3.println(strData);
    String atinit2 = "at+wsowr=0,";
    atinit2 += tr2.length();
    atinit2 += ",";
    atinit2 += tr2;
    Serial3.println(atinit2);

    //delay(200);
    //Serial3.print("at+wsowr=0,300,"+stringToHex("GET /HIENSYS/CatM1/charge_catm1.php?json="));
    //Serial3.print(stringToHex(jsonURL));
    
    //Serial3.println("AT+WSOWR=0,5,hello");
    //Serial3.println("+WSOWR:1,0");
    
}


void sendCatm1JSONURLEncoded(String jsonURL){
     jsonSent = jsonURL;
    Serial.println("sendCatm1JSONURLEncoded = "+jsonURL);

    //delay(100); // charge energy

    //String tr1 = stringToHex("GET /CatM1/socket2.php?token=");
    String tr1 = stringToHex("GET /CatM1/charge_token.php?token=");
    //String tr1 = stringToHex("GET /HIENSYS/CatM1/socket_test.php?json=");
    String tr2 = stringToHex(jsonURL);
    
    String tr3 = stringToHex(" HTTP/1.1");
    tr3+="0d0a";
    String tr4 = stringToHex("HOST: 13.125.162.166");
    tr4+="0d0a0d0a";
    String tr5 = "";//stringToHex("cache-control: no-cache");
    //tr5+="0d0a0d0a";
    
    //String tr3 = "20485454502f312e310d0a484f53543a207961636861776d2e6361666532342e636f6d0d0a63616368652d636f6e74726f6c3a206e6f2d63616368650d0a0d0a";
    int trSize = tr1.length()+tr2.length()+tr3.length()+tr4.length()+tr5.length();
    String atinit = "at+wsowr=0,";
    atinit += trSize;
    atinit += ",";
    atinit += tr1;
    atinit += tr2;

    String strData = "";
    strData += tr3;
    strData += tr4;
    strData += tr5;

    //Serial.println("AT$$DBS");
    //Serial3.println("AT$$DBS");

    //delay(200);
    
    Serial.print(atinit);
    Serial.println(strData);
    
    Serial3.print(atinit);
    Serial3.println(strData);

    String j1 = stringToHex("POST /CatM1/charge_catm1.php?json=");
    String j2 = stringToHex(jsonURL);
    String j3 = "20485454502f312e310d0a484f53543a207961636861776d2e6361666532342e636f6d0d0a63616368652d636f6e74726f6c3a206e6f2d63616368650d0a0d0a";

    int jSize = j1.length()+j2.length()+j3.length();

    String jinit = "at+wsowr=0,";
    jinit += jSize;
    jinit += ",";
    jinit += j1;
    jinit += j2;

//    Serial.print(jinit);
//    Serial.println(j3);
//
//    Serial3.print(jinit);
//    Serial3.println(j3);

//    Serial.print("at+wsowr=0,300,"+stringToHex("GET /HIENSYS/CatM1/charge_catm1.php?json="));
//    Serial.print(stringToHex(jsonURL));
//    Serial.println("20485454502f312e310d0a484f53543a207961636861776d2e6361666532342e636f6d0d0a63616368652d636f6e74726f6c3a206e6f2d63616368650d0a0d0a");
//
//    Serial3.print("at+wsowr=0,300,"+stringToHex("GET /HIENSYS/CatM1/charge_catm1.php?json="));
//    Serial3.print(stringToHex(jsonURL));
//    Serial3.println("20485454502f312e310d0a484f53543a207961636861776d2e6361666532342e636f6d0d0a63616368652d636f6e74726f6c3a206e6f2d63616368650d0a0d0a");

    
    
    //Serial3.println("AT+WSOWR=0,5,hello");
    //Serial3.println("+WSOWR:1,0");
    
}

//void sendCatm1JSONURLEncoded(String jsonURL){
//     jsonSent = jsonURL;
//    Serial.println("sendCatm1JSONURLEncoded = "+jsonURL);
//  
//    Serial3.print("at+wsowr=0,200,"+stringToHex("GET /HIENSYS/CatM1/charge_catm1.php?json="));
//    Serial3.print(stringToHex(jsonURL));
//    Serial3.println("20485454502f312e310d0a484f53543a207961636861776d2e6361666532342e636f6d0d0a63616368652d636f6e74726f6c3a206e6f2d63616368650d0a0d0a");
//}

void sendCurrent(){
  if(rfcheck){
    //String jsonString;

    //StaticJsonBuffer<300> jsonBuffer;
    //JsonObject& root = jsonBuffer.createObject();
    //JsonArray& data = root.createNestedArray("currents");
//    root["RF"] = tagID;
//    root["SE"] = serial;
//    root["BA"] = 100;
//    root["msg"] = "charging";
//    
    float tI = 0;
    float aI = 0;
    for(int i = 0 ; i < currentCount ; i++){
      tI += currents[i];
    }
    aI = tI/currentCount;
    //root["CU"] = aI;
    //data.add(aI);
    currentCount = 0;
    //root.printTo(jsonString);
    
    //String jsonEncodeString = urlencode(jsonString);

    String dataString = "";
    dataString += "+|";
    dataString += tagID;
    dataString += "|";
    dataString += serial;
    dataString += "|";
    dataString += testBattery;
    dataString += "|";
    dataString += "charging";//"charging";
    dataString += "|";
    dataString += testCurrent;
    dataString += "|+";

    testCurrent += 0.01;
    testBattery += 1;

    //sendCatm1JSONURLEncoded(jsonEncodeString);

    sendCatm1Token(dataString);
    
  }
}

void sendCurrentJson(){
  if(rfcheck){
    String jsonString;

    StaticJsonBuffer<300> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    //JsonArray& data = root.createNestedArray("currents");
    root["RF"] = tagID;
    root["SE"] = serial;
    root["BA"] = 100;
    root["msg"] = "charging";
    float tI = 0;
    float aI = 0;
    for(int i = 0 ; i < currentCount ; i++){
      tI += currents[i];
    }
    aI = tI/currentCount;
    root["CU"] = aI;
    //data.add(aI);
    currentCount = 0;
    root.printTo(jsonString);
    
    String jsonEncodeString = urlencode(jsonString);

    sendCatm1JSONURLEncoded(jsonEncodeString);
    
  }
}

void writeString(String stringData) { // Used to serially push out a String with Serial.write()

  for (int i = 0; i < stringData.length(); i++)
  {
    Serial.write(stringData[i]);   // Push each char 1 by 1 on each loop pass
  }

}// end writeString

bool isStringInclude(String fullString, String searchString){
  if(fullString.indexOf(searchString) != -1)return true;
  return false;
}

void setRelay(bool on){
  if(on){
    Serial.println("relay turn on");
    digitalWrite(8,HIGH);
  }
  else{
    Serial.println("relay turn off");
    digitalWrite(8,LOW);
    
  }
}

void catm1(){
  Serial.println("catm1");
  //Serial.println("c1");
  if (Serial3.available()){
    String r = Serial3.readString();
    writeString(r);
    //Serial.println("c1");
//    if(r == 'O'){
//      didReadO = true;
//    }
//    else if(didReadO && r == 'K'){
    //if(r=="\nOK"){

    if(csn==7 && (isStringInclude(r,"+WSORD=") || isStringInclude(r,"+wsowr="))){
      
      //currentPacketCount = 20;
      
      String rMsg = "";
      if(isStringInclude(r,"+WSORD=")){
        rMsg = convertMsg(r, "+WSORD=");
      }
      else{
        rMsg = convertMsg(r, "+wsowr=");
      }
      bool httpOK = false;
      if(isStringInclude(rMsg,"relay_on")){
        
        setRelay(1);
        httpOK = true;
      }
      else if(isStringInclude(rMsg,"relay_off")){
        
        setRelay(0);
        httpOK = true;
      }

      if(httpOK==false){
        testBattery = -10;
      }

      if(httpOK||isStringInclude(rMsg,"HTTP/1.1 200 OK")){
        
        csn = 6; // 응답을 받았으니 다시 송신모드로 변경
        tryCount[csn-1]=0;
        httpFailCount=0;
        tryCount[6]=0; // HTTP Fail 초기화
      }
      else{
        httpFailCount++;
        if(httpFailCount>2){ // 통신 오류가 3번 이상 발생하면,
          httpFailCount=0;

          resetCatM1();
          
          //Serial3.println("AT+CFUN=6"); // USIM 초기화 후
          //tStep(0);// 처음부터 다시 초기화
        }
        //sendCatm1JSONURLEncoded(jsonSent); // 응답은 받았는데 깨져 있으면 다시 송신
      }
      
      //delay(100);
      
    }
    //Serial.println("c2");
    //
    if(isStringInclude(r,"ALREADY_USED")){ // 이미 사용하고 있으면 리셋
      //delay(20000);
      //Serial3.println("AT+CFUN=6");
      resetCatM1();
    }
    if(isStringInclude(r,"DIS_IND")||isStringInclude(r,"READY")||isStringInclude(r,"ERROR")){
      //allowATC = true;
      setATC(true);
      csn = 0; // 
    }
    
    if(isStringInclude(r,"\nOK")){
      allowATC=true;
      if(csn==1){
        //delay(100);
        //cs2();
        tStep(csn);
      }
      else if(csn==2){
        //delay(100);
        tStep(csn);
        //cs3();
      }
      else if(csn==3){
        //delay(100);
        tStep(csn);
        //cs4();
      }
      else if(csn==4){
        //delay(100);
        tStep(csn);
        
      }
      else if(csn==5){
        //delay(100);
        tStep(csn);
        
      }
     //didReadO = false;
    }
    else{
      //didReadO = false;
    }
    
    //Serial.write(r);
    
  }

  //Serial.println("c3");
  handleCatm1Error();
  
  if(csn==0)tStep(0); // 

  /*if (Serial.available()){    
  
    Serial3.println("at+cereg?");
    delay(100);
    Serial3.println("at*rndisdata=1");
    delay(100);
    Serial3.println("at+wsocr=0,yachawm.cafe24.com,80,1,1");
    delay(100);
    Serial3.println("at+wsoco=0");
    delay(100);
    Serial3.print("at+wsowr=0,200,474554202f4849454e5359532f4361744d312f6d73672e7068703f6465766963655f69643d353535266d73673d"); //yachawm.cafe24.com
    Serial3.print(stringToHex(Serial.readString()));
    Serial3.println("20485454502f312e310d0a484f53543a207961636861776d2e6361666532342e636f6d0d0a63616368652d636f6e74726f6c3a206e6f2d63616368650d0a0d0a");
    
  } //cat M.1 �옉�뾽�빐�빞�븿 (RFID Data �쟾�넚)*/

  
  if(currentCount >= currentPacketCount && csn==6){ // 송신모드일때만 실행
    csn=7; // 수신모드로 변경
    sendCurrent();
  }
  //Serial.print("current = ");
  //Serial.println(Irms);
  //Serial.println("Count = "+currentCount);
}

void handleCatm1Error(){
  // error handle
  tryCount[csn-1]++;
  Serial.print(csn);
  Serial.print(":");
  Serial.print(tryCount[csn-1]);
  Serial.print(" ");
  if(csn==4){
    if( tryCount[csn-1] > tryDelay4){ // 서버 접속 초기화 오류일시 처음부터.
      allowATC = true;
      tStep(4);
    }
  }
  else if(csn==7){
    if(tryCount[csn-1] > tryDelay7){
      allowATC = true;
      tryCount[csn-1] = 0;
      //sendCatm1JSONURLEncoded(jsonSent);
      sendCatm1Token(tokenSent);
      //csn = 6; // 통신오류가 너무 심하면 전제 시스템이 지체되면 안되므로 재전송 무시.
    }
  }
  else if(retryCount[csn-1]>2 && csn!=6){ // 오류 공통 처리
    
      retryCount[csn-1] = 0;
      tryCount[csn-1] = 0;
      allowATC = true;
      //Serial3.println("AT+CFUN=6"); // USIM 초기화 후
      //tStep(0);// 처음부터 다시 초기화
      resetCatM1();
      //Serial.println("RESET");
    
    
  }
  else if(tryCount[csn-1]>tryDelay && csn!=6){ // 오류 공통 처리
    allowATC = true;
    Serial.println("Retry");
    tStep(csn-1);
    retryCount[csn-1]++;
  }
}


String urldecode(String str)
{
    
    String encodedString="";
    char c;
    char code0;
    char code1;
    for (int i =0; i < str.length(); i++){
        c=str.charAt(i);
      if (c == '+'){
        encodedString+=' ';  
      }else if (c == '%') {
        i++;
        code0=str.charAt(i);
        i++;
        code1=str.charAt(i);
        c = (h2int(code0) << 4) | h2int(code1);
        encodedString+=c;
      } else{
        
        encodedString+=c;  
      }
      
      yield();
    }
    
   return encodedString;
}

String urlencode(String str)
{
    String encodedString="";
    char c;
    char code0;
    char code1;
    char code2;
    for (int i =0; i < str.length(); i++){
      c=str.charAt(i);
      if (c == ' '){
        encodedString+= '+';
      } else if (isalnum(c)){
        encodedString+=c;
      } else{
        code1=(c & 0xf)+'0';
        if ((c & 0xf) >9){
            code1=(c & 0xf) - 10 + 'A';
        }
        c=(c>>4)&0xf;
        code0=c+'0';
        if (c > 9){
            code0=c - 10 + 'A';
        }
        code2='\0';
        encodedString+='%';
        encodedString+=code0;
        encodedString+=code1;
        //encodedString+=code2;
      }
      yield();
    }
    return encodedString;
    
}

unsigned char h2int(char c)
{
    if (c >= '0' && c <='9'){
        return((unsigned char)c - '0');
    }
    if (c >= 'a' && c <='f'){
        return((unsigned char)c - 'a' + 10);
    }
    if (c >= 'A' && c <='F'){
        return((unsigned char)c - 'A' + 10);
    }
    return(0);
}

void checkCurrent() {
  if(csn==6){ // 
    
    //Serial.println("checkCurrent");
    double Irms = 1.2345;  // Calculate Irms only
    //Serial.println(Irms);
    if(currentCount < currentPacketCount){
      currents[currentCount++] = Irms;
      //data.add(Irms);
    }
  }
}

void setATC(bool allow){
//  allowATC = allow;
//  if(allowATC){
//    digitalWrite(RTS, HIGH);
//    //digitalWrite(CTS, HIGH);
//  }
//  else{
//    digitalWrite(RTS, LOW);
//    //digitalWrite(CTS, LOW);
//  }
}

void loop() {

  if(csn!=7){ // 응답받고 있을 때 catm1 만 가동.
    //if(currentCount<2)delay(200);

    checkCurrent();
    //delay(30);
  }

  delay(100);
  if(act)catm1();
  if(csn==7){
    delay(10);
  }

}
